unit data;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, inifiles,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.FMXUI.Wait, Data.DB,
  FireDAC.Comp.Client, FMX.Forms, FireDAC.Comp.UI, FireDAC.DApt;

type
  Tdm = class(TDataModule)
    database1: TFDConnection;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure Tdm.DataModuleCreate(Sender: TObject);
Var servidor, ConStr, login, database : String;
    IniFile: TIniFile;
begin

  IniFile  := TIniFile.Create(System.SysUtils.GetCurrentDir+'\aplisoft.ini');
  Database := IniFile.ReadString('DATABASESERVER','DATABASENAME','');
  Servidor := IniFile.ReadString('DATABASESERVER','SQLSERVERNAME','');

  try
   if Database = '' then
     Raise Exception.Create('ERROR: al cargar Base de Datos');
  except
   on E: Exception do
    begin
      Application.ShowException(E);
    //  fAreError := true;
      Exit;
    end;
  end;

  try
    database1.Params.Values['Server']  := Servidor;
    database1.Params.Values['Database'] := Database;

  except
    on E: Exception do
    begin
      Application.ShowException(E.Create('Ha ocurrido un error al conectarse a la Base de Datos!'));
    //  fAreError := true;
      Exit;

    end;
  end;


end;

end.
