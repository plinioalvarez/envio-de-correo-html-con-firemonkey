unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.ScrollBox, FMX.Memo, FMX.Edit, FMX.Controls.Presentation, IdSSLOpenSSL,
  IdAttachmentFile, IdMessage, IdSmtp, System.Strutils, IdText,
  IdExplicitTLSClientServerBase, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FMX.ListBox, FMX.WebBrowser, inifiles, System.ImageList, FMX.ImgList,
  IdComponent, IdBaseComponent, IdTCPConnection, IdTCPClient, IdMessageClient,
  IdSMTPBase, FMX.Objects, Winapi.Windows, FMX.ComboEdit;


type
  TFMain = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EmailAddress: TEdit;
    Nota: TMemo;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    Memo1: TMemo;
    WebBrowser1: TWebBrowser;
    Button3: TButton;
    ImageList1: TImageList;
    Image1: TImage;
    Image2: TImage;
    ComboEdit1: TComboEdit;
    ComboEdit2: TComboEdit;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure ComboEdit1ClosePopup(Sender: TObject);
  private
    factura : string;
    { Private declarations }
    function CountStr(const ASearchFor, ASearchIn : string) : Integer;
    function SendMail(xSender, company, Reciever, subject, messageBody: string; Fileurl: string) : string;
    function createfacturahtml : string;
    function IsMatch(const Input, Pattern: string): boolean;
    function IsValidEmailRegEx(const EmailAddress: string): boolean;
    function EnDeCrypt(aStr: string; aKey: integer) : String;
    function clean:string;
  public
    { Public declarations }
  end;

var
  FMain: TFMain;


implementation
{$R *.fmx}

{ TFMain }

uses data, System.RegularExpressions;

procedure TFMain.Button1Click(Sender: TObject);
var resultado, correo, invalido : string;
    cnt, i : integer;
begin
  invalido := '';
 if EmailAddress.Text = '' then
    ShowMessage('Falta Correo')
  else
   begin
    correo := EmailAddress.text;
    cnt := CountStr(';' ,EmailAddress.text);
    for i := 1 to cnt do
     begin
      correo := copy(EmailAddress.Text,1,ansipos(';',EmailAddress.Text)-1);
      if not IsValidEmailRegEx(correo) then
          invalido := correo;
     end;
    if cnt = 0 then
     if not IsValidEmailRegEx(correo) then
       invalido := correo;

    if invalido = '' then
    begin
    resultado := SendMail('Suplidor', comboedit1.Items.Text, EmailAddress.text, 'Orden de Compra',
         factura,  memo1.Lines.Text);

    showmessage(resultado);
    end
   else
    showmessage(EmailAddress.text+' Correo Invalido');
   end;

end;

procedure TFMain.Button3Click(Sender: TObject);
begin
  clean;
end;

function TFMain.clean: string;
begin
  WebBrowser1.LoadFromStrings('','');
  memo1.Lines.Text := '';
  ComboEdit1.items.text := '';
  EmailAddress.Text := '';
  Nota.Text := '';
end;

procedure TFMain.ComboEdit1ClosePopup(Sender: TObject);
begin
   factura := createfacturahtml;
   comboedit2.ItemIndex := comboedit1.ItemIndex;
   if comboedit2.Items.Text <> '' then
      EmailAddress.Text := trim(comboedit2.Items.Text);
end;

function TFMain.CountStr(const ASearchFor, ASearchIn: string): Integer;
var Start : Integer;
begin
  Result := 0;
  Start := Pos(ASearchFor, ASearchIn);
  while Start > 0 do
    begin
      Inc(Result);
      Start := PosEx(ASearchFor, ASearchIn, Start + 1);
    end;
end;

function TFMain.createfacturahtml : string;
var qry : tfdquery;
    header, main, footer, primo : string;
    qtycnt, ofertacnt, pri : integer;
    sub, tot : double;
begin
  //item_code, barcode, rank_no, qty_orden, p_unit, qty_oferta, desc1, costo, total_prod
  sub := 0;
  tot := 0;
  qry := tfdquery.Create(self);
  qry.Connection := dm.database1;
  qry.sql.add('select top 20 * from vista_ordencompra_piar');
  qry.sql.add('where vend_code = '+QuotedStr(copy(comboEdit1.Items.Text, 1,4)));
//  qry.sql.add(' and qty_orden > 0 ');
  qry.open;
    header :=
    '<!DOCTYPE html>'+
    '<html lang="es">'+
    '<head>'+
    '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'+
    '<link rel="stylesheet" type="text/css" href="style.css" />'+
    '<meta name="viewport" content="width=device-width, initial-scale=1.0">'+
    '<meta http-equiv="X-UA-Compatible" content="ie=edge">'+
    '<title></title>'+
    '</head>'+
    '<body style="width: 900px;">'+
    '<div class="header">'+
    '<div style="text-align: center; font-size: 20px; font-weight: bold;">'+qry.FieldByName('co_name').Text+'</div>'+
    '<div style="text-align: center;">'+qry.FieldByName('rnc').Text+'</div>'+
    '<div style="text-align: center;">'+qry.FieldByName('co_phone1').Text+'/'+qry.FieldByName('co_phone2').Text+
   '</div>'+
   '</br>'+
   '<table style="table-layout:fixed; width: 900px;">'+
   '<tbody>'+
   '<tr>'+
   '<td style="color: gray; font-weight: bold; font-size: 15px; width: 120px;">Orden No</td>'+
   '<td style="color: black; font-size: 13px">: '+qry.FieldByName('no_orden').Text+'</td>'+
   '<td style="color: gray; font-weight: bold; font-size: 15px; width: 120px;">Fecha</td>'+
   '<td style="color: black; font-size: 13px">: '+qry.FieldByName('Fecha').Text+'</td>'+
   '</tr>'+
   '<tr>'+
   '<td style="color: gray; font-weight: bold; font-size: 15px; width: 120px;">Suplidor</td>'+
   '<td style="color: black; font-size: 13px">: '+comboEdit1.Items.text+'</td>'+
   '<td style="color: gray; font-weight: bold; font-size: 15px; width: 120px;">Terminos Dev</td>'+
   '<td style="color: black; font-size: 13px">: '+qry.FieldByName('terms_code').Text+'</td>'+
   '</tr>'+
   '<tr>'+
   '<td style="color: gray; font-weight: bold; font-size: 15px; width: 120px;">Direcci&oacute;n</td>'+
   '<td style="color: black; font-size: 13px">:</td>'+
   '<td style="color: gray; font-weight: bold; font-size: 15px; width: 120px;">Tel&eacute;fono(s)</td>'+
   '<td style="color: black; font-size: 13px">: '+qry.FieldByName('Phone').Text+'</td>'+
   '</tr>'+
   '<tr>'+
   '<td style="color: gray; font-weight: bold; font-size: 15px; width: 120px;">Contacto</td>'+
   '<td style="color: black; font-size: 13px">: '+qry.FieldByName('Contact').Text+'</td>'+
   '<td style="color: gray; font-weight: bold; font-size: 15px; width: 120px;">Celular/Beeper</td>'+
   '<td style="color: black; font-size: 13px">: '+qry.FieldByName('Cel_Cont').Text+'</td>'+
   '</tr>'+
   '</tbody>'+
   '</table>'+
   '</br>'+
   '</div>';


  qry.Last;
  while not qry.Bof do
   begin
    pri := pri + 1;
    if ( pri mod 2) = 0 then
      primo := 'style="background: #f1ecec69;"'
     else
      primo := '';

    main := main +
    '<tr '+primo+'>'+
     '<td style="width: 90px; text-align: left;">'+qry.FieldByName('item_code').Text+'</td>'+
     '<td style="width: 100px; text-align: left;">'+qry.FieldByName('barcode').Text+'</td>'+
     '<td style="width: 50px; text-align: center;">'+qry.FieldByName('cantidad').Text+'</td>'+
     '<td style="width: 50px; text-align: center;">'+qry.FieldByName('p_unit').Text+'</td>'+
     '<td style="width: 50px; text-align: center;">'+stringreplace(qry.FieldByName('qty_oferta').Text,'0','', [])+'</td>'+
     '<td style="width: 800px; text-align: left;">'+qry.FieldByName('desc1').Text+'</td>'+
     '<td style="width: 200px; text-align: right;">'+formatfloat(',0.00',qry.FieldByName('costo').AsFloat)+'</td>'+
     '<td style="width: 200px; text-align: right;">'+formatfloat(',0.00',qry.FieldByName('total_prod').AsFloat)+'</td>'+
     '</tr>';

     qtycnt   := qtycnt + qry.FieldByName('cantidad').AsInteger;
     ofertacnt:= ofertacnt + qry.FieldByName('qty_oferta').AsInteger;

     sub := sub + qry.FieldByName('total_prod').AsFloat;
     tot := tot + qry.FieldByName('total_prod').AsFloat;

    qry.Prior;
   end;

  main :=
    '<div class="main" style="font-size: 12px; table-layout: fixed; width: 900px;">'+
    '<table cellpadding="1" cellspacing="1" style="table-layout:fixed">'+
    '<tbody>'+
    '<tr style="width: 800px; background: #eaeaea;">'+
    '<th style="width: 40px; text-align: left;">C&oacute;digo</th>'+
    '<th style="width: 40px; text-align: left;">Barra</th>'+
    '<th style="width: 20px; text-align: center;">Cant</th>'+
    '<th style="width: 20px; text-align: center;">Unid</th>'+
    '<th style="width: 20px; text-align: center;">Oferta</th>'+
    '<th style="width: 800px; text-align: left;">Producto</th>'+
    '<th style="width: 200px; text-align: right;">Costo</th>'+
    '<th style="width: 200px; text-align: right;">Sub-Total</th>'+
    '</tr>'+
    main+
    '<tr>'+
    '<th>Total General</th>'+
    '<th></th>'+
    '<th style="text-align: center; border-top: 1px solid;">'+qtycnt.ToString+'</th>'+
    '<th></th>'+
    '<th style="text-align: center; border-top: 1px solid;">'+stringreplace(ofertacnt.ToString, '0', '', [])+'</th>'+
    '<th></th>'+
    '<th></th>'+
    '<th style="text-align: right; border-top: 1px solid;">'+formatfloat(',0.00',tot)+'</th>'+
    '</tr>'+
    '<tbody>'+
    '</table>'+
    '</div>';
    footer :=
    '</body>'+
    '</html>';


  factura := Header+main+footer;
  WebBrowser1.LoadFromStrings(factura,'');

  result := factura;

end;


function TFMain.EnDeCrypt(aStr : string; aKey: integer): String;
begin
   Result:='';
   RandSeed:=aKey;
   for aKey:=1 to Length(aStr) do
     Result:=Result+Chr(Byte(aStr[aKey]) xor random(256));
end;

procedure TFMain.FormCreate(Sender: TObject);
var qry : tfdquery;
    i : integer;
    suplidor : string;
begin
 //AniIndI.Visible := false;
  //  suplidor := '0075';

 for i := 1 to ParamCount do
    if suplidor = '' then
      suplidor := ''+ ParamStr(i)+''
     else
      suplidor := suplidor+', '''+ ParamStr(i) +'';

   qry := tfdquery.Create(self);
   qry.Connection := dm.database1;
   qry.sql.add('select distinct vend_code, bus_name, email_cont from vista_ordencompra_piar ');
   if suplidor <> '' then
     qry.sql.add('where vend_code in ('+suplidor+') ');

   qry.open();
   if suplidor <> '' then
      EmailAddress.Text := qry.Fields[2].Text;

   while not qry.eof do
    begin
     comboEdit1.Items.Add(qry.Fields[0].text+' '+qry.Fields[1].Text);
     comboEdit2.Items.Add(qry.Fields[2].text);
     qry.Next;
    end;

    if suplidor <> '' then
      begin
       comboEdit1.ItemIndex := 0;
       ComboEdit1ClosePopup(self);
      end;

    qry.Free;

end;

procedure TFMain.Image1Click(Sender: TObject);
begin
   if openDialog1.Execute then
      memo1.Lines.Add(openDialog1.Filename);
end;

function TFMain.IsMatch(const Input, Pattern: string): boolean;
begin
   Result := TRegEx.IsMatch(Input, Pattern);
end;

function TFMain.IsValidEmailRegEx(const EmailAddress: string): boolean;
const
  EMAIL_REGEX = '^((?>[a-zA-Z\d!#$%&''*+\-/=?^_`{|}~]+\x20*|"((?=[\x01-\x7f])'
             +'[^"\\]|\\[\x01-\x7f])*"\x20*)*(?<angle><))?((?!\.)'
             +'(?>\.?[a-zA-Z\d!#$%&''*+\-/=?^_`{|}~]+)+|"((?=[\x01-\x7f])'
             +'[^"\\]|\\[\x01-\x7f])*")@(((?!-)[a-zA-Z\d\-]+(?<!-)\.)+[a-zA-Z]'
             +'{2,}|\[(((?(?<!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d))'
             +'{4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\'
             +'[\x01-\x7f])+)\])(?(angle)>)$';
begin
  Result := IsMatch(EmailAddress, EMAIL_REGEX);
end;

function TFMain.SendMail(xSender, company, Reciever, subject, messageBody,
  Fileurl: string): string;
var IdSSLIOHandlerSocketOpenSSL1 : TIdSSLIOHandlersocketopenSSL;
    IndySmtp      : TIdSMTP;
    mailMensaje: TIdMessage;
    Adjunto : TIdAttachmentFile;
    i , val, key :integer;
    archivo:string;
    IniFile: TIniFile;
    IdText : TIdText;
    EUserName, EPassword, EHost, EPort, Cript : string;
begin
  key := 0;
  IniFile  := TIniFile.Create(System.SysUtils.GetCurrentDir+'\aplisoft.ini');
  EUserName :=  IniFile.ReadString('EMAIL','username','');
  EPassword :=  IniFile.ReadString('EMAIL','password','');
  EHost     :=  IniFile.ReadString('EMAIL','host','');
  EPort     :=  IniFile.ReadString('EMAIL','port','');

  if AnsiPos('Cript(', EPassword) > 0 then
     begin
      EPassword := copy(EPassword, 7, length(EPassword));
      EPassword := copy(EPassword, 1, length(EPassword)-1);
      EPassword := EnDeCrypt(Epassword, Key);
     end
    else
     begin
       Cript := EnDeCrypt(EPassword, key);
       IniFile.WriteString('EMAIL','password','Cript('+Cript+')');
     end;

  Fileurl := stringreplace(FileUrl, #$D#$A, ';',[]);
  if Fileurl <> '' then
     FileUrl := FileUrl+';';

  IndySmtp := TIdSMTP.Create(nil);
  mailMensaje := TIdMessage.Create(nil);
  IdSSLIOHandlerSocketOpenSSL1 := TIdSSLIOHandlersocketopenSSL.Create(nil);

   IdSSLIOHandlerSocketOpenSSL1.SSLOptions.Method      := sslvTLSv1;
   IdSSLIOHandlerSocketOpenSSL1.SSLOptions.Mode        := sslmUnassigned;
   IdSSLIOHandlerSocketOpenSSL1.SSLOptions.VerifyMode  := [];
   IdSSLIOHandlerSocketOpenSSL1.SSLOptions.VerifyDepth := 0;
   IdSSLIOHandlerSocketOpenSSL1.Host := EHost;
   IdSSLIOHandlerSocketOpenSSL1.Port := EPort.ToInteger;

  mailMensaje.Clear;
  mailMensaje.From.Name := COMPANY+' ('+xSender+')';
  mailMensaje.Recipients.EMailAddresses := reciever;
  mailMensaje.Subject := Subject;
  mailMensaje.Body.Text := '';
  mailMensaje.Priority := mpNormal;


  {Creo el cuerpo en TXT}
  IdText := TIdText.Create(mailmensaje.MessageParts);
  IdText.Body.Text := nota.Lines.Text;
  IdText.ContentType := 'text/plain';
  {Aqui creo el cuerpo en HTML}
  IdText := TIdText.Create(mailmensaje.MessageParts);
  IdText.Body.Text := messageBody;
  IdText.ContentType := 'text/html';

  {Creo el archivo adjunto}
  val :=  CountStr(';' ,Fileurl);
    for i := 1 to val do
      begin
        archivo := copy(Fileurl,1,ansipos(';',Fileurl)-1);
        Fileurl :=  stringreplace(Fileurl, archivo+';', '',[rfReplaceAll, rfIgnoreCase]);
        Adjunto  := TidAttachmentFile.Create(mailMensaje.MessageParts,archivo);
      end;

     IndySmtp.IOHandler := IdSSLIOHandlerSocketOpenSSL1;
     IndySmtp.UseTLS    := utUseExplicitTLS;
     IndySmtp.UserName  := EUserName;
     IndySmtp.Password  := EPassword;
     IndySmtp.Host      := Ehost;
     IndySmtp.Port      := EPort.ToInteger;

   try
     IndySmtp.AuthType  := satDefault;
     IndySmtp.Authenticate;
     IndySmtp.Connect;
    // mailMensaje.SaveToStream(tmpStream, false);
     IndySmtp.Send(mailMensaje);
     IndySmtp.Disconnect;
     result := ('Enviado Exitosamente!!!! ; '+Reciever);
  except
    Result := ('No se puedo Enviar el Mensaje');
  end;

  IndySmtp.DisposeOf;
  //if Adjunto <> nil then
 //   Adjunto.Free;
  IdSSLIOHandlerSocketOpenSSL1.Free;
  mailMensaje.Free;
  clean;

end;


end.
